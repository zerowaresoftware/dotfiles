all:
	echo "Choose a spefic target"

neovim: python
	# Install dependencies
	sudo apt-get install neovim python-neovim python3-neovim exuberant-ctags cscope ripgrep
	python3 -m pip install --user pynvim jedi yapf pylint
	mkdir -p ~/.config/nvim/colors
	mkdir -p ~/.config/nvim/plugin
	cp nvim/init.vim ~/.config/nvim/
	cp nvim/colors/winter.vim ~/.config/nvim/colors/winter.vim
	# Install vim-plug
	sh -c 'curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
	# Install Cscope
	wget http://cscope.sourceforge.net/cscope_maps.vim --directory-prefix=~/.config/nvim/plugin/
	echo "Remember to setup alias vim='nvim'"
	echo "Remember to enter in vim and run PlugInstall"

teminator:
	sudo apt-get install terminator i3

python:
	sudo apt-get install python3-pip python3-autopep8 pylint python3-venv
	python3 -m pip install --user pipx

git:
	sudo apt-get install git
	git config --global core.editor "vim"

terminal:
	echo "/usr/bin/xset r rate 300 50" >> ~/.bashrc
